import torch.nn as nn
import torch.nn.functional as F
from torchvision.transforms import GaussianBlur
class ResNet_Deblur(nn.Module):
    def __init__(self):
        super(ResNet_Deblur, self).__init__()
        
        self.conv1 = nn.Sequential(
            nn.Conv2d(in_channels=1, out_channels=64, kernel_size=3, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(2, return_indices=True)
        )
        self.res1 = nn.Sequential(nn.Sequential(
            nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True)
        ), nn.Sequential(
            nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True))
        )
        self.res2 = nn.Sequential(nn.Sequential(
            nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True)
        ), nn.Sequential(
            nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True))
        )
        self.conv2 = nn.Sequential(
            nn.Conv2d(64, 128, kernel_size=3, padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(2, return_indices=True)
        )
        self.res3 = nn.Sequential(nn.Sequential(
            nn.Conv2d(128, 128, kernel_size=3, padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU(inplace=True)
        ), nn.Sequential(
            nn.Conv2d(128, 128, kernel_size=3, padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU(inplace=True)
        ))
        self.res4 = nn.Sequential(nn.Sequential(
            nn.Conv2d(128, 128, kernel_size=3, padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU(inplace=True)
        ), nn.Sequential(
            nn.Conv2d(128, 128, kernel_size=3, padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU(inplace=True)
        ))
        self.conv3 = nn.Sequential(
            nn.Conv2d(128, 64, 3, padding=1),
            nn.ReLU(inplace=True)
        )
        self.conv4 = nn.Sequential(
            nn.Conv2d(64, 1, 3, padding=1)
        )
        
    def forward(self, x):
        x, indices1 = self.conv1(x)
        x = self.res1(x) + x
        x = self.res2(x) + x
        x, indices2 = self.conv2(x)
        x = self.res3(x) + x
        x = self.res4(x) + x
        x = F.max_unpool2d(x, indices2, 2)
        x = self.conv3(x)
        x = F.max_unpool2d(x, indices1, 2)
        x = self.conv4(x)
        return x
def getBlurFunc(kernal_size = 9, strength = 2):
    blur_func = GaussianBlur(kernal_size, strength)
    return blur_func
def getResNetDeblurModel():
    model = ResNet_Deblur()
    return model
