import torch.nn as nn
import torchvision.transforms.functional as TF
from torchvision.transforms import GaussianBlur
class deblur(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(1, 128, kernel_size=5, padding=2)
        self.conv2 = nn.Conv2d(128, 256, kernel_size=3, padding=1)
        self.btn1 = nn.BatchNorm2d(128)
        self.btn2 = nn.BatchNorm2d(256)
        self.relu = nn.ReLU(True)
        self.conv3 = nn.Conv2d(256, 64, kernel_size=3, padding=1)
        self.conv4 = nn.Conv2d(64, 1, kernel_size=3, padding=1)
    def forward(self, x):
        x = self.conv1(x)
        x = self.btn1(x)
        x = self.relu(x)
        x = self.conv2(x)
        x = self.btn2(x)
        x = self.relu(x)
        x = self.conv3(x)
        x = self.conv4(x)
        x = TF.adjust_contrast(x, 2)
        return x
def getBlurFunc(kernal_size = 9, strength = 2):
    blur_func = GaussianBlur(kernal_size, strength)
    return blur_func
     
def getDeblurModel():
    return deblur()