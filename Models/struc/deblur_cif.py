import torch.nn as nn
import torch.nn.functional as F
import torchvision.transforms.functional as TF
from torchvision.transforms import GaussianBlur

class deblur_cif(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(3, 128, kernel_size=3, padding=1)
        self.conv2 = nn.Conv2d(128, 256, kernel_size=3, padding=1)
        self.btn1 = nn.BatchNorm2d(128)
        self.btn2 = nn.BatchNorm2d(256)
        self.relu = nn.ReLU(True)
        self.conv3 = nn.Conv2d(256, 128, kernel_size=3, padding=1)
        self.conv4 = nn.Conv2d(128, 3, kernel_size=3, padding=1)
    def forward(self, x):
        x = F.dropout2d(self.conv1(x), training=self.training)
        x = self.btn1(x)
        x = self.relu(x)
        x = F.dropout2d(self.conv2(x), training=self.training)
        x = self.btn2(x)
        x = self.relu(x)
        x = F.dropout2d(self.conv3(x), training=self.training)
        x = self.relu(x)
        x = self.conv4(x)
        x = TF.adjust_contrast(x, 1.4)
        return x
def getBlurFunc(kernal_size = 7, strength = 1.8):
    blur_func = GaussianBlur(kernal_size, strength)
    return blur_func
 
def getCIFDeblurModel():
    return deblur_cif()