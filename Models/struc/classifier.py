import torch.nn as nn
import torch.nn.functional as F

class classifier(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(1, 32, kernel_size=5)
        self.conv2 = nn.Conv2d(32, 64, kernel_size=5)
        self.drop_conv2 = nn.Dropout2d()
        self.fnc1 = nn.Linear(1024, 400)
        self.fnc2 = nn.Linear(400, 50)
        self.fnc3 = nn.Linear(50, 10)
    def forward(self, x):
        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        x = F.relu(F.max_pool2d(self.drop_conv2(self.conv2(x)), 2))
        x = x.view(-1, 1024)
        x = F.relu(self.fnc1(x))
        x = F.dropout(x, training=self.training)
        x = F.relu(self.fnc2(x))
        x = F.dropout(x, training=self.training)
        x = self.fnc3(x)
        return F.log_softmax(x, dim=1)
def getClassModel():
    model = classifier()
    return model