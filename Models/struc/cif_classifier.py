import torch
import torch.nn as nn
import torch.nn.functional as F

class cif_classifier(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(3, 35, 5)
        self.conv2 = nn.Conv2d(35, 24, 5)
        self.drop_conv2 = nn.Dropout2d()
        self.fnc1 = nn.Linear(24 * 5 * 5, 357)
        self.fnc2 = nn.Linear(357, 123)
        self.fnc3 = nn.Linear(123, 84)
        self.fnc4 = nn.Linear(84, 10)
    def forward(self, x):
        x = F.max_pool2d(F.relu(self.conv1(x)), 2)
        x = F.max_pool2d(F.relu(self.conv2(x)), 2)
        x = x.view(-1, 24 * 5 * 5)
        x = F.relu(self.fnc1(x))
        x = F.dropout(x, training=self.training)
        x = F.relu(self.fnc2(x))
        x = F.dropout(x, training=self.training)
        x = F.relu(self.fnc3(x))
        x = F.dropout(x, training=self.training)
        x = self.fnc4(x)
        return F.log_softmax(x, dim=1)
       
def getClassModel():
    model = cif_classifier()
    return model