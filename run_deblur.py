import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import datasets
from torch.utils.data import DataLoader
from torchvision import transforms
from matplotlib import pyplot as plt
from pathlib import Path
from Code.Trainer.train import *
from Models.struc.deblur import getDeblurModel
from Models.struc.deblur import getBlurFunc
from Models.struc.classifier import getClassModel
from timeit import default_timer as Timer

def main():
    start = Timer()

    train = datasets.MNIST(root='./Images', 
                        train=True, 
                        download=True, 
                        transform=transforms.Compose([transforms.ToTensor()]))
    test = datasets.MNIST(root='./Images', 
                        train=False, 
                        download=True, 
                        transform=transforms.Compose([transforms.ToTensor()]))

    train_dataset = DataLoader(dataset=train,
                            batch_size=256,
                            shuffle=True,
                            num_workers=4)

    test_dataset = DataLoader(dataset=test,
                            batch_size=256,
                            shuffle=True,
                            num_workers=4)
    
    evaluation_dataset = DataLoader(dataset=test,
                                    batch_size=1,
                                    shuffle=True,
                                    num_workers=4)

    if torch.cuda.is_available():
        device = torch.device('cuda')
        print('device: cuda')
    else:
        device = torch.device('mps')
        print('device: mps')

    deblur_model = getDeblurModel()
    deblur_model.train(True)
    class_model = getClassModel()
    class_model.load_state_dict(torch.load('./Models/saves/classifier.pth', map_location=device))
    class_model.eval()
    opt = torch.optim.Adam(deblur_model.parameters(), 0.0001)
    epochs = 45
    blur_func = getBlurFunc()
    
    
    losses = [[],[]]

    for x in range(epochs):
        print(f'epoch: {x+1} of {epochs}')
        deblur_model.train(True)
        train_loss = train_deblur(deblur_model, device, train_dataset, opt, blur_func)
        deblur_model.eval()
        test_loss = test_deblur(deblur_model, device, test_dataset, blur_func)
        print('train:{:.2f}, test:{:.2f}'.format(train_loss, test_loss))
        losses[0].append(train_loss)
        losses[1].append(test_loss)
    
    plt.plot(losses[0], label='training')
    plt.plot(losses[1], label='testing')
    plt.yscale('log')
    plt.legend(title='losses')
    plt.savefig('./Images/Figures/deblur_loss_output.png')
    plt.show()
    end = Timer()
    time = end - start
    print('Process took: {:.2f}'.format(time/60))
    
    deblur_model.eval()
    acc = deblur_acr(class_model, deblur_model, device, evaluation_dataset, blur_func)
    print('The model is: {:.2f}% accurate'.format(acc))

    """p = Path()
    p = p.resolve()
    p = str(p)
    for i, data in enumerate(evaluation_dataset):
        img, label = data
        deblur_model.to(device)
        img, label = img.to(device), label.to(device)
        if i < 3:
           str_1 = 'blurred_{}.png'.format(i+1)
           str_2 = 'output_{}.png'.format(i+1)
           blurred = blur_func(img)
           output = deblur_model(blurred)
           to_pil = transforms.ToPILImage()
           blurred = to_pil(blurred.cpu().detach().squeeze())
           output = to_pil(output.cpu().detach().squeeze())
           blurred.save(p + '/Outputs/Images/MNIST/' + str_1)
           output.save(p + '/Outputs/Images/MNIST/' + str_2)
        else:
            break"""


    torch.save(deblur_model.state_dict(), './Models/saves/deblur.pth')

    if(torch.cuda.is_available()):
        torch.cuda.empty_cache()
    
if __name__ == '__main__':
    main()
