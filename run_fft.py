import torch
from torchvision import datasets
from torchvision import transforms
from torch.utils.data import DataLoader
from torch.optim.lr_scheduler import ExponentialLR
from Models.struc.resnet import getResNetModel
from Models.struc.deblur_cif import getBlurFunc
from rfft_res import getModel
from Code.Trainer.train import *
def load_dataset():
    train = datasets.CIFAR10(root='./Images', 
                        train=True, 
                        download=True, 
                        transform=transforms.Compose([transforms.ToTensor(),
                                                      transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]))
    test = datasets.CIFAR10(root='./Images', 
                        train=False, 
                        download=True, 
                        transform=transforms.Compose([transforms.ToTensor(),
                                                      transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]))

    train_dataset = DataLoader(dataset=train,
                            batch_size=64,
                            shuffle=True,
                            num_workers=4)

    test_dataset = DataLoader(dataset=test,
                            batch_size=64,
                            shuffle=True,
                            num_workers=4)
    
    evaluation_dataset = DataLoader(dataset=test,
                                    batch_size=1,
                                    shuffle=True,
                                    num_workers=4)
    return train_dataset, test_dataset, evaluation_dataset

def main():
    cModel = getResNetModel()
    dbModel = getModel()
    bFunc = getBlurFunc()
    dbModel.train()
    if (torch.cuda.is_available()):
        device = torch.device('cuda')
    else:
        device = torch.device('mps')
    
    cModel.load_state_dict(torch.load('./Models/saves/cif_resnet_classifier.pth', device))
    
    cModel = cModel.to(device)
    dbModel = dbModel.to(device)
    bFunc = bFunc.to(device)
    
    opt = torch.optim.Adam(dbModel.parameters(), 0.0001)
    epochs = 100
    model_best = 0
    train_set, test_set, eval_set = load_dataset()

    cModel.eval()
    
    class_check = classifier_acr(cModel, device, eval_set)
    print(f'classifier is {class_check}% accurate')
    
    for i in range(epochs):
        print(f'{i} of {epochs}')
        dbModel.train()
        loss = train_deblur(dbModel, device, train_set, opt, bFunc)
        print(f'{loss:.6f}')
        if (i % 10 == 9):
            dbModel.eval()
            acc = deblur_acr(cModel, dbModel, device, eval_set, bFunc)
            if (acc > model_best):
                model_best = acc
                torch.save(dbModel.state_dict(), './Models/saves/cif_deblur_rfft.pth')
                print(f'Model is {acc:.2f}% accurate')
    if(torch.cuda.is_available()):
        torch.cuda.empty_cache()



if __name__ == '__main__':
    main()

 
