import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import datasets
from torch.utils.data import DataLoader
from torchvision import transforms
from matplotlib import pyplot as plt
from pathlib import Path
from Code.Trainer.train import *
from Models.struc.deblur_cif import getCIFDeblurModel
from Models.struc.deblur_cif import getBlurFunc
from timeit import default_timer as Timer

def main():
    start = Timer()

    train = datasets.CIFAR10(root='./Images', 
                        train=True, 
                        download=True, 
                        transform=transforms.Compose([transforms.ToTensor(),
                            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]))
    test = datasets.CIFAR10(root='./Images', 
                        train=False, 
                        download=True, 
                        transform=transforms.Compose([transforms.ToTensor(),
                            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]))

    train_dataset = DataLoader(dataset=train,
                            batch_size=128,
                            shuffle=True,
                            num_workers=4)

    test_dataset = DataLoader(dataset=test,
                            batch_size=128,
                            shuffle=True,
                            num_workers=4)
    
    evaluation_dataset = DataLoader(dataset=test,
                                    batch_size=1,
                                    shuffle=True,
                                    num_workers=4)

    if torch.cuda.is_available():
        device = torch.device('cuda')
        print('device: cuda')
    else:
        device = torch.device('mps')
        print('device: mps')

    cif_deblur_model = getCIFDeblurModel()
    cif_deblur_model.train(True)
    opt = torch.optim.Adam(cif_deblur_model.parameters(), 0.0001)
    epochs = 100
    blur_func = getBlurFunc()
    
    
    losses = [[],[]]

    for x in range(epochs):
        print(f'epoch: {x+1} of {epochs}')
        cif_deblur_model.train(True)
        train_loss = train_deblur(cif_deblur_model, device, train_dataset, opt, blur_func)
        cif_deblur_model.eval()
        test_loss = test_deblur(cif_deblur_model, device, test_dataset, blur_func)
        print('train:{:.2f}, test:{:.2f}'.format(train_loss, test_loss))
        losses[0].append(train_loss)
        losses[1].append(test_loss)
    
    plt.plot(losses[0], label='training')
    plt.plot(losses[1], label='testing')
    plt.yscale('log')
    plt.legend(title='losses')
    plt.savefig('./Images/Figures/cif_deblur_loss_output.png')
    plt.show()
    end = Timer()
    time = end - start
    print('Process took: {:.2f}'.format(time/60))


    torch.save(cif_deblur_model.state_dict(), './Models/saves/cif_deblur.pth')
    
    p = Path()
    p = p.resolve()
    p = str(p)
    for i, data in enumerate(evaluation_dataset):
        img, label = data
        cif_deblur_model.to(device)
        img, label = img.to(device), label.to(device)
        if i < 3:
           str_1 = 'blurred_{}.png'.format(i+1)
           str_2 = 'output_{}.png'.format(i+1)
           blurred = blur_func(img)
           output = cif_deblur_model(blurred)
           to_pil = transforms.ToPILImage()
           blurred = to_pil(blurred.cpu().detach().squeeze())
           output = to_pil(output.cpu().detach().squeeze())
           blurred.save(p + '/Outputs/Images/CIF/' + str_1)
           output.save(p + '/Outputs/Images/CIF/' + str_2)
        else:
            break



    if(torch.cuda.is_available()):
        torch.cuda.empty_cache()
    
if __name__ == '__main__':
    main()
