import torch
import torch.nn.functional as F
from Utility.pgbar import printProgressBar

def train_classifier(model, device, data_set, opt):
    model.to(device)
    epoch_loss = 0

    for i, data in enumerate(data_set):
        img, label = data
        img, label = img.to(device), label.to(device) 

        img.requires_grad = True
        opt.zero_grad()

        output = model(img)
        loss = F.cross_entropy(output, label)
        epoch_loss += loss.item()

        loss.backward()
        opt.step()
        printProgressBar(i, len(data_set))
    print()
    return epoch_loss/len(data_set)
        
def test_classifier(model, device, data_set):
    model.to(device)
    epoch_loss = 0

    for i, data in enumerate(data_set):
        img, label = data
        img, label = img.to(device), label.to(device) 

        output = model(img)
        loss = F.cross_entropy(output, label)
        epoch_loss += loss.item()
        printProgressBar(i, len(data_set))
    print()
    return epoch_loss/len(data_set)

def classifier_acr(model, device, data_set):
    model.to(device)
    correct = 0
    
    for i, data in enumerate(data_set):
        img, label = data
        img, label = img.to(device), label.to(device)
        
        output = model(img)
        init_pred = output.max(1, keepdim=True)[1]

        printProgressBar(i, len(data_set))
        if init_pred.item() == label.item():
            correct += 1
    print()
    return (correct/len(data_set)) * 100

def train_deblur(model, device, data_set, opt, blur_func):
    model.to(device)
    epoch_loss = 0

    for i, data in enumerate(data_set):
        org_img, label = data
        blur_img = blur_func(org_img)
        blur_img, org_img = blur_img.to(device), org_img.to(device) 

        blur_img.requires_grad = True
        opt.zero_grad()

        output = model(blur_img)
        loss = F.mse_loss(output, org_img)
        epoch_loss += loss.item()

        loss.backward()
        opt.step()
        printProgressBar(i, len(data_set))
    print()
    return epoch_loss/len(data_set)
 
def test_deblur(model, device, data_set, blur_func):
    with torch.no_grad():
        model.to(device)
        epoch_loss = 0

        for i, data in enumerate(data_set):
            org_img, label = data
            blur_img = blur_func(org_img)
            org_img, blur_img = org_img.to(device), blur_img.to(device)

            output = model(blur_img)
            loss = F.mse_loss(output, org_img)
            epoch_loss += loss.item()
            del loss, org_img, blur_img, output
            printProgressBar(i, len(data_set))
        print()
        return epoch_loss/len(data_set)

def deblur_acr(class_model, deblur_model, device, data_set, blur_func):
    class_model.to(device)
    deblur_model.to(device)
    correct = 0
    
    for i, data in enumerate(data_set):
        org_img, label = data
        blur_img = blur_func(org_img)
        org_img, blur_img = org_img.to(device), blur_img.to(device)
        
        output = deblur_model(blur_img)
        output = class_model(output)
        init_pred = output.max(1, keepdim=True)[1]

        if init_pred.item() == label.item():
            correct += 1
        printProgressBar(i, len(data_set))
    print()
    return (correct/len(data_set)) * 100

def cif_classifier_acr(class_model, deblur_model, device, data_set, blur_func):
    class_model.to(device)
    deblur_model.to(device)
    correct = 0
    
    for i, data in enumerate(data_set):
        img, label = data
        img, label = img.to(device), label.to(device)
        blurred = blur_func(img)
        unblurred = deblur_model(blurred)
        
        output = class_model(unblurred)
        init_pred = output.max(1, keepdim=True)[1]

        if init_pred.item() == label.item():
            correct += 1
    return (correct/len(data_set)) * 100
