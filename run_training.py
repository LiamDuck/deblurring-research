import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import datasets
from torch.utils.data import DataLoader
from torchvision import transforms
from matplotlib import pyplot as plt
from Code.Trainer.train import *
from Models.struc.classifier import getClassModel
from timeit import default_timer as Timer

def main():
    start = Timer()

    train = datasets.MNIST(root='./Images', 
                        train=True, 
                        download=True, 
                        transform=transforms.Compose([transforms.ToTensor()]))
    test = datasets.MNIST(root='./Images', 
                        train=False, 
                        download=True, 
                        transform=transforms.Compose([transforms.ToTensor()]))

    train_dataset = DataLoader(dataset=train,
                            batch_size=128,
                            shuffle=True,
                            num_workers=4)

    test_dataset = DataLoader(dataset=test,
                            batch_size=128,
                            shuffle=True,
                            num_workers=4)
    
    evaluation_dataset = DataLoader(dataset=test,
                                    batch_size=1,
                                    shuffle=True,
                                    num_workers=4)

    if torch.cuda.is_available():
        device = torch.device('cuda')
        print('device: cuda')
    else:
        device = torch.device('mps')
        print('device: mps')

    model = getClassModel()
    model.train(True)
    opt = torch.optim.Adam(model.parameters(), 0.0001)
    epochs = 30
    
    
    losses = [[],[]]

    for x in range(epochs):
        print(f'epoch: {x+1} of {epochs}')
        model.train(True)
        train_loss = train_classifier(model, device, train_dataset, opt)
        model.eval()
        test_loss = test_classifier(model, device, test_dataset)
        print('train:{:.2f}, test:{:.2f}'.format(train_loss, test_loss))
        losses[0].append(train_loss)
        losses[1].append(test_loss)
    
    plt.plot(losses[0], label='training')
    plt.plot(losses[1], label='testing')
    plt.yscale('log')
    plt.legend(title='losses')
    plt.savefig('./Images/Figures/loss_output.png')
    end = Timer()
    time = end - start
    print('Process took: {:.2f}'.format(time/60))
    
    acc = classifier_acr(model, device, evaluation_dataset)
    print('The model is: {:.2f}% accurate'.format(acc))

    #torch.save(model.state_dict(), './Models/saves/classifier.pth')
    
if __name__ == '__main__':
    main()
