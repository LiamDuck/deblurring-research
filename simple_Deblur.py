import torch
import torchvision
from torchvision import transforms
from torchvision import datasets
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
from simple import Deblur1Chan
from torch.utils.data import DataLoader

train = datasets.MNIST("./Images", 
                       True,
                       transforms.Compose([transforms.ToTensor()]),
                       download=True)
test = datasets.MNIST("./Images",
                      False,
                      transforms.Compose([transforms.ToTensor()]),
                      download=True)
train = DataLoader(train, 128, True)
test_loader = DataLoader(test, 128, True)
test_eval = DataLoader(test, 1, True)

blur = torchvision.transforms.GaussianBlur(3)
train_data = []
for data, label in train:
    blur_data = blur(data)
    train_data.append((data, blur_data, label))

test_eval_data = []
for data, label in test_eval:
    blur_data = blur(data)
    test_eval_data.append((data, blur_data, label))

device = torch.device("mps")
model_1 = Deblur1Chan()

opt = torch.optim.Adam(model_1.parameters(), lr=0.001)
scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau( 
        opt,
        mode='min',
        patience=5,
        factor=0.5,
        verbose=True
    )
epochs = 1
model_1.to(device)
for i in range(epochs):
    model_1.train()
    epoch_loss = 0
    for data, blur, label in train_data:
        data = data.to(device)
        blur = blur.to(device)
        label = label.to(device)

        opt.zero_grad()
        pred = model_1(blur)
        loss = F.mse_loss(pred, data)
        loss.backward()
        opt.step()
        epoch_loss += loss.item()
    training_loss = epoch_loss/len(train_data)
    print(training_loss)
    
i = 0
for data, blur, label in test_eval_data:
    blur = blur.to(device)
    output = model_1(blur)
    plt.imshow(output.cpu().detach().squeeze())
    if i == 0:
        break

