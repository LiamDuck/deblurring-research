import sys
from Code.DataSets.load_data import *
from Code.Trainer.train import *
from Models.struc.classifier import getClassModel as getMnistClassModel
from Models.struc.resnet import getResNetModel as getCIFClassModel
from Models.struc.mnist_resnet_deblur import getBlurFunc as getMnistBlurFunc
from Models.struc.mnist_resnet_deblur import getResNetDeblurModel as getMnistDeblurModel
from Models.struc.cif_resnet_deblur import getCifResNetDeblurModel
from Models.struc.cif_resnet_deblur import getBlurFunc as getCIFDeblurFunc


def parseArgs(argInputs, device):
    arguments = argInputs[1:]
    if arguments[1] == "-mnist":
        model = getMnistClassModel()
        deblur_func = getMnistBlurFunc()
        deblur_model = getMnistDeblurModel()
        data = mnist()
        if arguments[0] == "-class" and arguments[2] == "-train":
            return(model, data, True, "mnist")
        elif arguments[0] == "-class" and arguments[2] == "-test":
            model.load_state_dict(torch.load('./Models/saves/mnist_classifier.pth', device))
            return(model, data, False, "mnist")
        elif arguments[0] == "-deblur" and arguments[2] == "-train":
            model.load_state_dict(torch.load('./Models/saves/mnist_classifier.pth', device))
            return(model, deblur_model, deblur_func, data, True, "mnist")
        elif arguments[0] == "-deblur" and arguments[2] == "-test":
            model.load_state_dict(torch.load('./Models/saves/mnist_classifier.pth', device))
            deblur_model.load_state_dict(torch.load('./Models/saves/mnist_deblur.pth', device))
            return(model, deblur_model, deblur_func, data, False, "mnist")
    elif arguments[1] == "-cifar":
        model = getCIFClassModel()
        deblur_func = getCIFDeblurFunc()
        deblur_model = getCifResNetDeblurModel()
        data = cifar10()
        if arguments[0] == "-class" and arguments[2] == "-train":
            return(model, data, True, "cifar")
        elif arguments[0] == "-class" and arguments[2] == "-test":
            model.load_state_dict(torch.load('./Models/saves/cif_classifier.pth', device))
            return(model, data, False, "cifar")
        elif arguments[0] == "-deblur" and arguments[2] == "-train":
            model.load_state_dict(torch.load('./Models/saves/cif_classifier.pth', device))
            return(model, deblur_model, deblur_func, data, True, "cifar")
        elif arguments[0] == "-deblur" and arguments[2] == "-test":
            model.load_state_dict(torch.load('./Models/saves/cif_classifier.pth', device))
            deblur_model.load_state_dict(torch.load('./Models/saves/cif_deblur.pth', device))
            return(model, deblur_model, deblur_func, data, False, "cifar")
   

    
def main():
    epochs = 100
    if (torch.cuda.is_available()):
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')
        
    output = parseArgs(sys.argv, device)
    if len(output) == 6:
        model, deblur_model, blur_func, data, is_training, save_where = output
        train_set, test_set, eval_set = data
        if is_training:
            best = 0
            opt = torch.optim.Adam(deblur_model.parameters(), 0.0001)
            for i in range(epochs):
                print(f'{i+1} of {epochs}')
                deblur_model.train()
                train_loss = train_deblur(deblur_model, device, train_set, opt, blur_func)
                deblur_model.eval()
                test_loss = test_deblur(deblur_model, device, test_set, blur_func)
                print(f'training loss:{train_loss:.6f}')
                print(f'testing loss:{test_loss:.6f}')
                if (i % 10 == 9):
                    deblur_model.eval()
                    acc = deblur_acr(model, deblur_model, device, eval_set, blur_func)
                    print(f"model is {acc}% accurate")
                    if acc > best:
                        best = acc
                        match save_where:
                            case "cifar":
                                torch.save(deblur_model.state_dict(), './Models/saves/cif_deblur.pth')
                            case "mnist":
                                torch.save(deblur_model.state_dict(), "./Models/saves/mnist_deblur.pth")
        else:
            model.eval()
            deblur_model.eval()
            acc = deblur_acr(model, deblur_model, device, eval_set, blur_func)
            print(f"model is {acc:.2}% accurate")           
    else:
        model, data, is_training, save_where = output
        train_set, test_set, eval_set = data
        if is_training:
            opt = torch.optim.Adam(model.parameters(), 0.00003) 
            best = 0
            for i in range(epochs):
                print(f'{i+1} of {epochs}')
                model.train()
                train_loss = train_classifier(model, device, train_set, opt)
                model.eval()
                test_loss = test_classifier(model, device, test_set)
                print(f'training loss:{train_loss:.6f}')
                print(f'testing loss:{test_loss:.6f}')
                if (i % 10 == 9):
                    model.eval()
                    acc = classifier_acr(model, device, eval_set)
                    print(f"model is {acc}% accurate")
                    if acc > best:
                        best = acc
                        match save_where:
                            case "cifar":
                                torch.save(model.state_dict(), './Models/saves/cif_classifier.pth')
                            case "mnist":
                                torch.save(model.state_dict(), './Models/saves/mnist_classifier.pth')
        else:
            model.eval()
            acc = classifier_acr(model, device, eval_set)
            print(f"model is {acc:.2}% accurate")

    
if __name__ == '__main__':
    main()
