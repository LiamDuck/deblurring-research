import torch
import torch.nn as nn
from torch.optim.lr_scheduler import ExponentialLR
import torch.nn.functional as F
from torchvision import datasets
from torch.utils.data import DataLoader
from torchvision import transforms
from matplotlib import pyplot as plt
from pathlib import Path
from Code.Trainer.train import *
from Models.struc.resnet import getResNetModel
from timeit import default_timer as Timer

def main():
    start = Timer()

    train = datasets.CIFAR10(root='./Images', 
                        train=True, 
                        download=True, 
                        transform=transforms.Compose([transforms.ToTensor(),
                                    transforms.RandomCrop(32, padding=4, padding_mode='reflect'),
                                    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]))
    test = datasets.CIFAR10(root='./Images', 
                        train=False, 
                        download=True, 
                        transform=transforms.Compose([transforms.ToTensor(),
                                    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]))

    train_dataset = DataLoader(dataset=train,
                            batch_size=128,
                            shuffle=True,
                            num_workers=4)

    test_dataset = DataLoader(dataset=test,
                            batch_size=128,
                            shuffle=True,
                            num_workers=4)
    
    evaluation_dataset = DataLoader(dataset=test,
                                    batch_size=1,
                                    shuffle=True,
                                    num_workers=4)

    if torch.cuda.is_available():
        device = torch.device('cuda')
        print('device: cuda')
    else:
        device = torch.device('mps')
        print('device: mps')

    cif_classifier = getResNetModel()
    cif_classifier.train(True)
    opt = torch.optim.Adam(cif_classifier.parameters(), 0.00001)
    epochs = 150
    best_acc = 0
    
    losses = [[],[]]

    for x in range(epochs):
        print(f'epoch: {x+1} of {epochs}')
        cif_classifier.train(True)
        train_loss = train_classifier(cif_classifier, device, train_dataset, opt)
        cif_classifier.eval()
        test_loss = test_classifier(cif_classifier, device, test_dataset)
        print('train:{:.4f}, test:{:.4f}'.format(train_loss, test_loss))
        losses[0].append(train_loss)
        losses[1].append(test_loss)
        if x % 10 == 9:
            acc = classifier_acr(cif_classifier, device, evaluation_dataset)
            print('The model is: {:.2f}% accurate'.format(acc))
            if acc > best_acc:
                torch.save(cif_classifier.state_dict(), './Models/saves/cif_resnet_classifier.pth')
                

    plt.plot(losses[0], label='training')
    plt.plot(losses[1], label='testing')
    plt.yscale('log')
    plt.legend(title='losses')
    plt.savefig('./Images/Figures/cif_classifier_loss_output.png')
    end = Timer()
    time = end - start
    print('Process took: {:.2f}'.format(time/60))

    if(torch.cuda.is_available()):
        torch.cuda.empty_cache()
    
if __name__ == '__main__':
    main()
